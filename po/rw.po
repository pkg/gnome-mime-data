# translation of gnome-mime-data to Kinyarwanda.
# Copyright (C) 2005 Free Software Foundation, Inc.
# This file is distributed under the same license as the gnome-mime-data package.
# Steve Murphy <murf@e-tools.com>, 2005
# Steve performed initial rough translation from compendium built from translations provided by the following translators:
# Philibert Ndandali  <ndandali@yahoo.fr>, 2005.
# Viateur MUGENZI <muvia1@yahoo.fr>, 2005.
# Noëlla Mupole <s24211045@tuks.co.za>, 2005.
# Carole Karema <karemacarole@hotmail.com>, 2005.
# JEAN BAPTISTE NGENDAHAYO <ngenda_denis@yahoo.co.uk>, 2005.
# Augustin KIBERWA  <akiberwa@yahoo.co.uk>, 2005.
# Donatien NSENGIYUMVA <ndonatienuk@yahoo.co.uk>, 2005..
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-mime-data HEAD\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-03-30 23:29-0700\n"
"PO-Revision-Date: 2004-11-04 10:13-0700\n"
"Last-Translator: Steve Murphy <murf@e-tools.com>\n"
"Language-Team: Kinyarwanda <translation-team-rw@lists.sourceforge.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: gnome-vfs.keys.in.h:1
#, fuzzy
msgid "2D chemical structure"
msgstr "Imiterere"

#: gnome-vfs.keys.in.h:2
#, fuzzy
msgid "3D Studio image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:3
msgid "AIFC audio"
msgstr ""

#: gnome-vfs.keys.in.h:4
msgid "AIFF audio"
msgstr ""

#: gnome-vfs.keys.in.h:5
#, fuzzy
msgid "ANIM animation"
msgstr "Iyega"

#: gnome-vfs.keys.in.h:6
msgid "ARJ archive"
msgstr ""

#: gnome-vfs.keys.in.h:7
#, fuzzy
msgid "AVI video"
msgstr "Videwo..."

#: gnome-vfs.keys.in.h:8
#, fuzzy
msgid "AbiWord document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:9
#, fuzzy
msgid "Active Server page"
msgstr "Ipaji"

#: gnome-vfs.keys.in.h:10
msgid "Address card"
msgstr ""

#: gnome-vfs.keys.in.h:11
#, fuzzy
msgid "Adobe FrameMaker font"
msgstr "Intego- nyuguti"

#: gnome-vfs.keys.in.h:12
#, fuzzy
msgid "Adobe font metrics"
msgstr "Intego- nyuguti Bijyanye n'ipima"

#: gnome-vfs.keys.in.h:13
msgid "Andrew Toolkit inset"
msgstr ""

#: gnome-vfs.keys.in.h:14
msgid "Application launcher"
msgstr ""

#: gnome-vfs.keys.in.h:15
#, fuzzy
msgid "ApplixWare Graphics image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:16
#, fuzzy
msgid "Applixware Words document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:17
#, fuzzy
msgid "Applixware presentation"
msgstr "Iyerekana"

#: gnome-vfs.keys.in.h:18
#, fuzzy
msgid "Applixware spreadsheet"
msgstr "Urupapuro rusesuye"

#: gnome-vfs.keys.in.h:19
msgid "Ar archive"
msgstr ""

#: gnome-vfs.keys.in.h:20
#, fuzzy
msgid "Assembly source code"
msgstr "Inkomoko ITEGEKONGENGA"

#: gnome-vfs.keys.in.h:21
msgid "Audio"
msgstr ""

#: gnome-vfs.keys.in.h:22
#, fuzzy
msgid "Authors list"
msgstr "Urutonde"

#: gnome-vfs.keys.in.h:23
#, fuzzy
msgid "AutoCAD image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:24
#, fuzzy
msgid "BCPIO document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:25
#, fuzzy
msgid "BDF font"
msgstr "Intego- nyuguti"

# 503
#: gnome-vfs.keys.in.h:26
msgid "Backup file"
msgstr "inyibutsabubiko"

#: gnome-vfs.keys.in.h:27
msgid "Basic audio"
msgstr ""

#: gnome-vfs.keys.in.h:28
#, fuzzy
msgid "Bibliography record"
msgstr "Icyabitswe"

#: gnome-vfs.keys.in.h:29
#, fuzzy
msgid "Binary program"
msgstr "Porogaramu"

#: gnome-vfs.keys.in.h:30
#, fuzzy
msgid "BitTorrent seed file"
msgstr "IDOSIYE"

#: gnome-vfs.keys.in.h:31
#, fuzzy
msgid "Blender file"
msgstr "IDOSIYE"

#: gnome-vfs.keys.in.h:32
#, fuzzy
msgid "Block device"
msgstr "APAREYE"

#: gnome-vfs.keys.in.h:33
#, fuzzy
msgid "Bzip2 compressed file"
msgstr "Byegeranijwe IDOSIYE"

#: gnome-vfs.keys.in.h:34
#, fuzzy
msgid "C shell script"
msgstr "C Igikonoshwa IYANDIKA"

#: gnome-vfs.keys.in.h:35
#, fuzzy
msgid "C source code"
msgstr "C Inkomoko ITEGEKONGENGA"

#: gnome-vfs.keys.in.h:36
#, fuzzy
msgid "C source code header"
msgstr "C Inkomoko ITEGEKONGENGA Umutwempangano"

#: gnome-vfs.keys.in.h:37
#, fuzzy
msgid "C++ source code"
msgstr "C Inkomoko ITEGEKONGENGA"

#: gnome-vfs.keys.in.h:38
#, fuzzy
msgid "CGI program"
msgstr "Porogaramu"

#: gnome-vfs.keys.in.h:39
#, fuzzy
msgid "CGM image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:40
#, fuzzy
msgid "CMU raster image"
msgstr "AGAPANDE Ishusho"

#: gnome-vfs.keys.in.h:41
msgid "CPIO archive"
msgstr ""

#: gnome-vfs.keys.in.h:42
#, fuzzy
msgid "CPIO archive (Gzip-compressed)"
msgstr "Byegeranijwe"

#: gnome-vfs.keys.in.h:43
#, fuzzy
msgid "Calendar file"
msgstr "IDOSIYE"

#: gnome-vfs.keys.in.h:44
#, fuzzy
msgid "Calendar or event document"
msgstr "Cyangwa Icyabaye Inyandiko"

#: gnome-vfs.keys.in.h:45
#, fuzzy
msgid "Character device"
msgstr "APAREYE"

#: gnome-vfs.keys.in.h:46
#, fuzzy
msgid "Cinelerra editing sheet"
msgstr "URUPAPURO"

#: gnome-vfs.keys.in.h:47
#, fuzzy
msgid "Comma-separated text document"
msgstr "Umwandiko Inyandiko"

#: gnome-vfs.keys.in.h:48
msgid "Commodore 64 audio"
msgstr ""

#: gnome-vfs.keys.in.h:49
#, fuzzy
msgid "Compound document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:50
#, fuzzy
msgid "Compress-compressed file"
msgstr "Byegeranijwe IDOSIYE"

#: gnome-vfs.keys.in.h:51
#, fuzzy
msgid "Compressed GIMP document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:52
#, fuzzy
msgid "Corel Draw drawing"
msgstr "Igishushanyo"

#: gnome-vfs.keys.in.h:53
#, fuzzy
msgid "Crystalline structure model"
msgstr "Imiterere Urugero"

#: gnome-vfs.keys.in.h:54
#, fuzzy
msgid "DCL script"
msgstr "IYANDIKA"

#: gnome-vfs.keys.in.h:55
#, fuzzy
msgid "DOS font"
msgstr "Intego- nyuguti"

#: gnome-vfs.keys.in.h:56
#, fuzzy
msgid "DOS/Windows program"
msgstr "Porogaramu"

#: gnome-vfs.keys.in.h:57
#, fuzzy
msgid "DSSSL document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:58
#, fuzzy
msgid "DV video"
msgstr "Videwo..."

#: gnome-vfs.keys.in.h:59
#, fuzzy
msgid "DXF vector graphic"
msgstr "Igishushanyo"

#: gnome-vfs.keys.in.h:60
msgid "Debian package"
msgstr ""

#: gnome-vfs.keys.in.h:61
msgid "Device Independant Bitmap"
msgstr ""

#: gnome-vfs.keys.in.h:62
msgid "Dia diagram"
msgstr ""

#: gnome-vfs.keys.in.h:63
#, fuzzy
msgid "Digital Imaging and Communications in Medicine image"
msgstr "Na in Ishusho"

#: gnome-vfs.keys.in.h:64
#, fuzzy
msgid "Digital Moving Picture Exchange image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:65
#, fuzzy
msgid "Directory information file"
msgstr "Ibisobanuro IDOSIYE"

#: gnome-vfs.keys.in.h:66
#, fuzzy
msgid "DjVu image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:67
#, fuzzy
msgid "Document type definition"
msgstr "Ubwoko Insobanuro"

#: gnome-vfs.keys.in.h:68
msgid "Documents"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:69
msgid "Documents/Diagram"
msgstr ""

#: gnome-vfs.keys.in.h:70
msgid "Documents/Extended Markup Language (XML)"
msgstr ""

#: gnome-vfs.keys.in.h:71
msgid "Documents/Numeric"
msgstr ""

#: gnome-vfs.keys.in.h:72
msgid "Documents/Plain Text"
msgstr ""

#: gnome-vfs.keys.in.h:73
msgid "Documents/Presentation"
msgstr ""

#: gnome-vfs.keys.in.h:74
msgid "Documents/Project Management"
msgstr ""

#: gnome-vfs.keys.in.h:75
msgid "Documents/Published Materials"
msgstr ""

#: gnome-vfs.keys.in.h:76
msgid "Documents/Spreadsheet"
msgstr ""

#: gnome-vfs.keys.in.h:77
msgid "Documents/TeX"
msgstr ""

#: gnome-vfs.keys.in.h:78
msgid "Documents/Text Markup"
msgstr ""

#: gnome-vfs.keys.in.h:79
msgid "Documents/Vector Graphics"
msgstr ""

#: gnome-vfs.keys.in.h:80
msgid "Documents/Word Processor"
msgstr ""

#: gnome-vfs.keys.in.h:81
msgid "Documents/World Wide Web"
msgstr ""

#: gnome-vfs.keys.in.h:82
msgid "Dolby Digital audio"
msgstr ""

#: gnome-vfs.keys.in.h:83
msgid "Dreamcast ROM"
msgstr ""

#: gnome-vfs.keys.in.h:84
#, fuzzy
msgid "Emacs Lisp source code"
msgstr "Inkomoko ITEGEKONGENGA"

#: gnome-vfs.keys.in.h:85
#, fuzzy
msgid "Email headers"
msgstr "Imitwe"

#: gnome-vfs.keys.in.h:86
#, fuzzy
msgid "Email message/mailbox"
msgstr "Ubutumwa"

#: gnome-vfs.keys.in.h:87
#, fuzzy
msgid "Encrypted message"
msgstr "Ubutumwa"

#: gnome-vfs.keys.in.h:88
msgid "Enlightenment theme"
msgstr ""

#: gnome-vfs.keys.in.h:89
#, fuzzy
msgid "Enriched text document"
msgstr "Umwandiko Inyandiko"

#: gnome-vfs.keys.in.h:90
#, fuzzy
msgid "Epiphany bookmarks file"
msgstr "Ibirango IDOSIYE"

#: gnome-vfs.keys.in.h:91
msgid "FLAC audio"
msgstr ""

#: gnome-vfs.keys.in.h:92
#, fuzzy
msgid "FLC animation"
msgstr "Iyega"

#: gnome-vfs.keys.in.h:93
#, fuzzy
msgid "FLI animation"
msgstr "Iyega"

#: gnome-vfs.keys.in.h:94
msgid "FastTracker II audio"
msgstr ""

#: gnome-vfs.keys.in.h:95
msgid "FlashPix Image"
msgstr ""

#: gnome-vfs.keys.in.h:96
msgid "Flexible Image Transport System"
msgstr ""

# sfx2/source\explorer\explorer.src:STR_SFX_FOLDER.text
#: gnome-vfs.keys.in.h:97
msgid "Folder"
msgstr "Ububiko"

#: gnome-vfs.keys.in.h:98
#, fuzzy
msgid "Fortran source code"
msgstr "Inkomoko ITEGEKONGENGA"

#: gnome-vfs.keys.in.h:99
#, fuzzy
msgid "FrameMaker interchange document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:100
#, fuzzy
msgid "G3 fax image"
msgstr "Fagisi Ishusho"

#: gnome-vfs.keys.in.h:101
#, fuzzy
msgid "GIF image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:102
#, fuzzy
msgid "GIMP document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:103
#, fuzzy
msgid "GMC link"
msgstr "Ihuza"

#: gnome-vfs.keys.in.h:104
#, fuzzy
msgid "GNOME desktop theme"
msgstr "Ibiro"

#: gnome-vfs.keys.in.h:105
msgid "GNU Oleo Spreadsheet"
msgstr ""

#: gnome-vfs.keys.in.h:106
#, fuzzy
msgid "GNU mail message"
msgstr "Ubutumwa Ubutumwa"

#: gnome-vfs.keys.in.h:107
#, fuzzy
msgid "GTK configuration"
msgstr "Iboneza"

#: gnome-vfs.keys.in.h:108
msgid "Game Boy ROM"
msgstr ""

#: gnome-vfs.keys.in.h:109
msgid "Genesis ROM"
msgstr ""

#: gnome-vfs.keys.in.h:110
#, fuzzy
msgid "Glade project"
msgstr "Umushinga"

#: gnome-vfs.keys.in.h:111
msgid "GnuCash workbook"
msgstr ""

#: gnome-vfs.keys.in.h:112
#, fuzzy
msgid "Gnumeric spreadsheet"
msgstr "Urupapuro rusesuye"

#: gnome-vfs.keys.in.h:113
msgid "Gtar archive"
msgstr ""

#: gnome-vfs.keys.in.h:114
msgid "Gtktalog Catalogue"
msgstr ""

#: gnome-vfs.keys.in.h:115
#, fuzzy
msgid "Gzip-compressed file"
msgstr "Byegeranijwe IDOSIYE"

#: gnome-vfs.keys.in.h:116
#, fuzzy
msgid "HDF document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:117
#, fuzzy
msgid "HTML page"
msgstr "Ipaji"

#: gnome-vfs.keys.in.h:118
#, fuzzy
msgid "Haskell source code"
msgstr "Inkomoko ITEGEKONGENGA"

#: gnome-vfs.keys.in.h:119
#, fuzzy
msgid "Help page"
msgstr "Ipaji"

#: gnome-vfs.keys.in.h:120
#, fuzzy
msgid "IDL document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:121
#, fuzzy
msgid "IEF image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:122
#, fuzzy
msgid "IFF image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:123
#, fuzzy
msgid "ILBM image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:124
#, fuzzy
msgid "ISI video"
msgstr "Videwo..."

#: gnome-vfs.keys.in.h:125
#, fuzzy
msgid "ISO image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:126
msgid "Images"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:127
msgid "Impulse Tracker audio"
msgstr ""

#: gnome-vfs.keys.in.h:128
msgid "Information"
msgstr "Ibisobanuro"

#: gnome-vfs.keys.in.h:129
msgid "Information/Calendar"
msgstr ""

#: gnome-vfs.keys.in.h:130
msgid "Information/Financial"
msgstr ""

#: gnome-vfs.keys.in.h:131
#, fuzzy
msgid "Installed GNOME desktop theme"
msgstr "Ibiro"

#: gnome-vfs.keys.in.h:132
msgid "JBuilder Project"
msgstr ""

#: gnome-vfs.keys.in.h:133
#, fuzzy
msgid "JPEG image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:134
#, fuzzy
msgid "Java byte code"
msgstr "Bayite ITEGEKONGENGA"

#: gnome-vfs.keys.in.h:135
#, fuzzy
msgid "Java code archive"
msgstr "ITEGEKONGENGA"

#: gnome-vfs.keys.in.h:136
#, fuzzy
msgid "Java source code"
msgstr "Inkomoko ITEGEKONGENGA"

#: gnome-vfs.keys.in.h:137
#, fuzzy
msgid "JavaScript source code"
msgstr "Inkomoko ITEGEKONGENGA"

#: gnome-vfs.keys.in.h:138
#, fuzzy
msgid "KDE application details"
msgstr "Porogaramu Birambuye"

#: gnome-vfs.keys.in.h:139
#, fuzzy
msgid "KIllustrator document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:140
#, fuzzy
msgid "KPresenter presentation"
msgstr "Iyerekana"

#: gnome-vfs.keys.in.h:141
#, fuzzy
msgid "KSpread spreadsheet"
msgstr "Urupapuro rusesuye"

#: gnome-vfs.keys.in.h:142
#, fuzzy
msgid "KWord document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:143
#, fuzzy
msgid "Korn shell script"
msgstr "Igikonoshwa IYANDIKA"

#: gnome-vfs.keys.in.h:144
msgid "LHA archive"
msgstr ""

#: gnome-vfs.keys.in.h:145
msgid "LHARC archive"
msgstr ""

#: gnome-vfs.keys.in.h:146
#, fuzzy
msgid "LIBGRX font"
msgstr "Intego- nyuguti"

#: gnome-vfs.keys.in.h:147
#, fuzzy
msgid "LightWave object"
msgstr "Igikoresho"

#: gnome-vfs.keys.in.h:148
msgid "LightWave scene"
msgstr ""

#: gnome-vfs.keys.in.h:149
#, fuzzy
msgid "Linux PSF console font"
msgstr "Intego- nyuguti"

#: gnome-vfs.keys.in.h:150
#, fuzzy
msgid "Literate haskell source code"
msgstr "Inkomoko ITEGEKONGENGA"

#: gnome-vfs.keys.in.h:151
#, fuzzy
msgid "Lotus 1-2-3 spreadsheet"
msgstr "1. 2. 3. Urupapuro rusesuye"

#: gnome-vfs.keys.in.h:152
#, fuzzy
msgid "LyX document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:153
msgid "MIDI audio"
msgstr ""

#: gnome-vfs.keys.in.h:154
msgid "MOD audio"
msgstr ""

#: gnome-vfs.keys.in.h:155
msgid "MP3 audio"
msgstr ""

#: gnome-vfs.keys.in.h:156
msgid "MP3 audio playlist"
msgstr ""

#: gnome-vfs.keys.in.h:157
#, fuzzy
msgid "MPEG video"
msgstr "Videwo..."

#: gnome-vfs.keys.in.h:158
#, fuzzy
msgid "MPEG-4 audio"
msgstr "4."

#: gnome-vfs.keys.in.h:159
msgid "MS ASF audio"
msgstr ""

#: gnome-vfs.keys.in.h:160
#, fuzzy
msgid "MS ASF video"
msgstr "Videwo..."

#: gnome-vfs.keys.in.h:161
#, fuzzy
msgid "MS video"
msgstr "Videwo..."

#: gnome-vfs.keys.in.h:162
msgid "MSX ROM"
msgstr ""

#: gnome-vfs.keys.in.h:163
#, fuzzy
msgid "MacBinary file"
msgstr "IDOSIYE"

#: gnome-vfs.keys.in.h:164
#, fuzzy
msgid "Macintosh AppleDouble-encoded file"
msgstr "IDOSIYE"

#: gnome-vfs.keys.in.h:165
#, fuzzy
msgid "Macintosh BinHex-encoded file"
msgstr "IDOSIYE"

#: gnome-vfs.keys.in.h:166
msgid "Macintosh StuffIt archive"
msgstr ""

#: gnome-vfs.keys.in.h:167
#, fuzzy
msgid "Macromedia Flash file"
msgstr "IDOSIYE"

#: gnome-vfs.keys.in.h:168
#, fuzzy
msgid "MagicPoint presentation"
msgstr "Iyerekana"

#: gnome-vfs.keys.in.h:169
#, fuzzy
msgid "Magick image format"
msgstr "Ishusho Imiterere"

#: gnome-vfs.keys.in.h:170
#, fuzzy
msgid "Mail delivery report"
msgstr "Icyegeranyo"

#: gnome-vfs.keys.in.h:171
#, fuzzy
msgid "Mail disposition report"
msgstr "Icyegeranyo"

#: gnome-vfs.keys.in.h:172
#, fuzzy
msgid "Mail system report"
msgstr "Sisitemu Icyegeranyo"

#: gnome-vfs.keys.in.h:173
msgid "Makefile"
msgstr ""

#: gnome-vfs.keys.in.h:174
#, fuzzy
msgid "Manual page"
msgstr "Ipaji"

#: gnome-vfs.keys.in.h:175
#, fuzzy
msgid "Manual page (compressed)"
msgstr "Ipaji Byegeranijwe"

#: gnome-vfs.keys.in.h:176
#, fuzzy
msgid "Master System or Game Gear ROM"
msgstr "Cyangwa"

# svtools/source\misc\imagemgr.src:STR_DESCRIPTION_MATHML_DOC.text
#: gnome-vfs.keys.in.h:177
#, fuzzy
msgid "MathML document"
msgstr "Inyandiko ya MathML"

#: gnome-vfs.keys.in.h:178
#, fuzzy
msgid "Matroska video"
msgstr "Videwo..."

#: gnome-vfs.keys.in.h:179
msgid "Memory dump"
msgstr ""

# sfx2/source\doc\doc.src:STR_DOCTYPENAME_MESSAGE.text
#: gnome-vfs.keys.in.h:180
msgid "Message"
msgstr "Ubutumwa"

#: gnome-vfs.keys.in.h:181
msgid "Message digest"
msgstr ""

#: gnome-vfs.keys.in.h:182
#, fuzzy
msgid "Message in several formats"
msgstr "in Imiterere"

#: gnome-vfs.keys.in.h:183
#, fuzzy
msgid "Microsoft Excel spreadsheet"
msgstr "Urupapuro rusesuye"

#: gnome-vfs.keys.in.h:184
#, fuzzy
msgid "Microsoft PowerPoint document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:185
msgid "Microsoft WMV playlist"
msgstr ""

#: gnome-vfs.keys.in.h:186
#, fuzzy
msgid "Microsoft WMV video"
msgstr "Videwo..."

# setup2/source\custom\reg4msdoc\preg4msdoc.src:STR_MS_WORD_DOCUMENT.text
#: gnome-vfs.keys.in.h:187
#, fuzzy
msgid "Microsoft Word document"
msgstr "Inyandiko ya Microsoft Word"

#: gnome-vfs.keys.in.h:188
#, fuzzy
msgid "Microsoft video"
msgstr "Videwo..."

#: gnome-vfs.keys.in.h:189
msgid "Monkey audio"
msgstr ""

#: gnome-vfs.keys.in.h:190
#, fuzzy
msgid "Mozilla bookmarks file"
msgstr "Mozilla Ibirango IDOSIYE"

#: gnome-vfs.keys.in.h:191
#, fuzzy
msgid "Multi-part message"
msgstr "Ubutumwa"

#: gnome-vfs.keys.in.h:192
msgid "NES ROM"
msgstr ""

#: gnome-vfs.keys.in.h:193
msgid "Named pipe"
msgstr ""

#: gnome-vfs.keys.in.h:194
#, fuzzy
msgid "Nautilus link"
msgstr "Ihuza"

#: gnome-vfs.keys.in.h:195
#, fuzzy
msgid "Netscape bookmarks file"
msgstr "Netscape Ibirango IDOSIYE"

#: gnome-vfs.keys.in.h:196
msgid "Nintendo64 ROM"
msgstr ""

#: gnome-vfs.keys.in.h:197
#, fuzzy
msgid "Nullsoft video"
msgstr "Videwo..."

#: gnome-vfs.keys.in.h:198
#, fuzzy
msgid "ODA document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:199
#, fuzzy
msgid "Object code"
msgstr "ITEGEKONGENGA"

#: gnome-vfs.keys.in.h:200
#, fuzzy
msgid "Objective C source code"
msgstr "C Inkomoko ITEGEKONGENGA"

#: gnome-vfs.keys.in.h:201
msgid "Ogg audio"
msgstr ""

#: gnome-vfs.keys.in.h:202
#, fuzzy
msgid "OpenOffice.org Impress presentation"
msgstr "OpenOffice."

#: gnome-vfs.keys.in.h:203
#, fuzzy
msgid "OpenOffice.org Impress presentation template"
msgstr "OpenOffice."

#: gnome-vfs.keys.in.h:204
#, fuzzy
msgid "OpenOffice.org Math document"
msgstr "OpenOffice."

#: gnome-vfs.keys.in.h:205
#, fuzzy
msgid "OpenOffice.org Writer document"
msgstr "OpenOffice."

#: gnome-vfs.keys.in.h:206
#, fuzzy
msgid "OpenOffice.org Writer global document"
msgstr "OpenOffice."

#: gnome-vfs.keys.in.h:207
#, fuzzy
msgid "OpenOffice.org Writer template"
msgstr "OpenOffice."

#: gnome-vfs.keys.in.h:208
#, fuzzy
msgid "OpenOffice.org drawing"
msgstr "OpenOffice."

#: gnome-vfs.keys.in.h:209
#, fuzzy
msgid "OpenOffice.org drawing template"
msgstr "OpenOffice."

#: gnome-vfs.keys.in.h:210
#, fuzzy
msgid "OpenOffice.org spreadsheet"
msgstr "OpenOffice."

#: gnome-vfs.keys.in.h:211
#, fuzzy
msgid "OpenOffice.org spreadsheet template"
msgstr "OpenOffice."

#: gnome-vfs.keys.in.h:212
#, fuzzy
msgid "OpenType font"
msgstr "Intego- nyuguti"

#: gnome-vfs.keys.in.h:213
#, fuzzy
msgid "PBM image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:214
#, fuzzy
msgid "PC Paintbrush image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:215
#, fuzzy
msgid "PCF font"
msgstr "Intego- nyuguti"

#: gnome-vfs.keys.in.h:216
#, fuzzy
msgid "PDF document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:217
#, fuzzy
msgid "PEF program"
msgstr "Porogaramu"

#: gnome-vfs.keys.in.h:218
#, fuzzy
msgid "PGM image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:219
msgid "PGN chess game"
msgstr ""

#: gnome-vfs.keys.in.h:220
#, fuzzy
msgid "PGP keys"
msgstr "Utubuto"

#: gnome-vfs.keys.in.h:221
#, fuzzy
msgid "PGP message"
msgstr "Ubutumwa"

#: gnome-vfs.keys.in.h:222
#, fuzzy
msgid "PGP signature"
msgstr "Isinya"

#: gnome-vfs.keys.in.h:223
#, fuzzy
msgid "PGP-encrypted file"
msgstr "Bishunzwe: IDOSIYE"

#: gnome-vfs.keys.in.h:224
#, fuzzy
msgid "PHP script"
msgstr "IYANDIKA"

#: gnome-vfs.keys.in.h:225
#, fuzzy
msgid "PICT image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:226
#, fuzzy
msgid "PNG image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:227
#, fuzzy
msgid "PNM image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:228
#, fuzzy
msgid "PPM image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:229
msgid "Packages"
msgstr ""

#: gnome-vfs.keys.in.h:230
#, fuzzy
msgid "Palm OS database"
msgstr "Ububikoshingiro"

#: gnome-vfs.keys.in.h:231
#, fuzzy
msgid "Palm Pixmap image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:232
#, fuzzy
msgid "Partial email message"
msgstr "imeli Ubutumwa"

#: gnome-vfs.keys.in.h:233
#, fuzzy
msgid "Pascal source code"
msgstr "Inkomoko ITEGEKONGENGA"

#: gnome-vfs.keys.in.h:234
#, fuzzy
msgid "Perl script"
msgstr "IYANDIKA"

#: gnome-vfs.keys.in.h:235
#, fuzzy
msgid "Photo CD image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:236
#, fuzzy
msgid "Photoshop document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:237
#, fuzzy
msgid "Plain text document"
msgstr "Umwandiko Inyandiko"

#: gnome-vfs.keys.in.h:238
msgid "Playlist"
msgstr ""

#: gnome-vfs.keys.in.h:239
#, fuzzy
msgid "PostScript Type 1 font"
msgstr "1. Intego- nyuguti"

#: gnome-vfs.keys.in.h:240
#, fuzzy
msgid "PostScript document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:241
#, fuzzy
msgid "Profiler results"
msgstr "Ibisubizo ku"

#: gnome-vfs.keys.in.h:242
msgid "Project Plan"
msgstr ""

#: gnome-vfs.keys.in.h:243
#, fuzzy
msgid "Python byte code"
msgstr "Bayite ITEGEKONGENGA"

#: gnome-vfs.keys.in.h:244
#, fuzzy
msgid "Python source code"
msgstr "Inkomoko ITEGEKONGENGA"

#: gnome-vfs.keys.in.h:245
msgid "QuickTime movie"
msgstr ""

#: gnome-vfs.keys.in.h:246
#, fuzzy
msgid "Quicken document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:247
#, fuzzy
msgid "Quicken for Windows document"
msgstr "kugirango Inyandiko"

#: gnome-vfs.keys.in.h:248
msgid "RAR archive"
msgstr ""

#: gnome-vfs.keys.in.h:249
#, fuzzy
msgid "README document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:250
#, fuzzy
msgid "RGB image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:251
msgid "RIFF audio"
msgstr ""

#: gnome-vfs.keys.in.h:252
msgid "RPM package"
msgstr ""

#: gnome-vfs.keys.in.h:253
msgid "Raw Gray Sample"
msgstr ""

#: gnome-vfs.keys.in.h:254
#, fuzzy
msgid "RealAudio document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:255
#, fuzzy
msgid "RealAudio/Video document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:256
#, fuzzy
msgid "RealVideo video"
msgstr "Videwo..."

#: gnome-vfs.keys.in.h:257
#, fuzzy
msgid "Reference to remote file"
msgstr "Kuri IDOSIYE"

#: gnome-vfs.keys.in.h:258
#, fuzzy
msgid "Rejected patch file"
msgstr "IDOSIYE"

#: gnome-vfs.keys.in.h:259
#, fuzzy
msgid "Rich text document"
msgstr "Umwandiko Inyandiko"

#: gnome-vfs.keys.in.h:260
#, fuzzy
msgid "S/MIME file"
msgstr "IDOSIYE"

#: gnome-vfs.keys.in.h:261
#, fuzzy
msgid "S/MIME signature"
msgstr "Isinya"

#: gnome-vfs.keys.in.h:262
#, fuzzy
msgid "SGI video"
msgstr "Videwo..."

#: gnome-vfs.keys.in.h:263
#, fuzzy
msgid "SGML document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:264
msgid "SHOUTcast playlist"
msgstr ""

#: gnome-vfs.keys.in.h:265
#, fuzzy
msgid "SMIL script"
msgstr "IYANDIKA"

#: gnome-vfs.keys.in.h:266
#, fuzzy
msgid "SQL code"
msgstr "ITEGEKONGENGA"

# officecfg/registry\schema\org\openoffice\Office\Impress.xcs:....Filter.Export.LastUsed..RAS_-_Sun_Rasterfile.text
#: gnome-vfs.keys.in.h:267
#, fuzzy
msgid "SUN Rasterfile"
msgstr "Agapandedosiye Zuba"

#: gnome-vfs.keys.in.h:268
msgid "SV4 CPIO archive"
msgstr ""

#: gnome-vfs.keys.in.h:269
#, fuzzy
msgid "SV4 CPIP archive (with CRC)"
msgstr "Na:"

#: gnome-vfs.keys.in.h:270
msgid "SVG art"
msgstr ""

#: gnome-vfs.keys.in.h:271
msgid "Samba share"
msgstr ""

#: gnome-vfs.keys.in.h:272
#, fuzzy
msgid "Scheme source code"
msgstr "Inkomoko ITEGEKONGENGA"

#: gnome-vfs.keys.in.h:273
#, fuzzy
msgid "Scream Tracker 3 audio"
msgstr "3."

#: gnome-vfs.keys.in.h:274
msgid "Scream Tracker audio"
msgstr ""

#: gnome-vfs.keys.in.h:275
msgid "Scream Tracker instrument"
msgstr ""

#: gnome-vfs.keys.in.h:276
#, fuzzy
msgid "Search results"
msgstr "ibisubizo by'ishakisha"

# offmgr/source\offapp\dialog\treeopt.src:RID_OFADLG_OPTIONS_TREE_PAGES.SID_GENERAL_OPTIONS.11.text
#: gnome-vfs.keys.in.h:277
msgid "Security"
msgstr "Umutekano"

#: gnome-vfs.keys.in.h:278
#, fuzzy
msgid "Setext document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:279
msgid "Shared Printer"
msgstr ""

#: gnome-vfs.keys.in.h:280
#, fuzzy
msgid "Shared library"
msgstr "Isomero"

#: gnome-vfs.keys.in.h:281
msgid "Shell archive"
msgstr ""

#: gnome-vfs.keys.in.h:282
#, fuzzy
msgid "Shell script"
msgstr "IYANDIKA"

#: gnome-vfs.keys.in.h:283
#, fuzzy
msgid "Signed message"
msgstr "Ubutumwa"

#: gnome-vfs.keys.in.h:284
#, fuzzy
msgid "Silicon Graphics image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:285
msgid "Socket"
msgstr ""

#: gnome-vfs.keys.in.h:286
msgid "Software Development"
msgstr ""

#: gnome-vfs.keys.in.h:287
msgid "Software Development/ROM Images"
msgstr ""

#: gnome-vfs.keys.in.h:288
msgid "Software Development/Source Code"
msgstr ""

#: gnome-vfs.keys.in.h:289
#, fuzzy
msgid "Software author credits"
msgstr "Umwanditsi"

#: gnome-vfs.keys.in.h:290
#, fuzzy
msgid "Software installation instructions"
msgstr "iyinjizaporogaramu Amabwiriza"

#: gnome-vfs.keys.in.h:291
msgid "Software license terms"
msgstr ""

#: gnome-vfs.keys.in.h:292
#, fuzzy
msgid "Source code patch"
msgstr "ITEGEKONGENGA"

#: gnome-vfs.keys.in.h:293
#, fuzzy
msgid "Speech document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:294
#, fuzzy
msgid "Speedo font"
msgstr "Intego- nyuguti"

#: gnome-vfs.keys.in.h:295
#, fuzzy
msgid "Spreadsheet Interchange document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:296
msgid "Stampede package"
msgstr ""

#: gnome-vfs.keys.in.h:297
#, fuzzy
msgid "StarCalc spreadsheet"
msgstr "Urupapuro rusesuye"

#: gnome-vfs.keys.in.h:298
#, fuzzy
msgid "StarChart chart"
msgstr "Igishushanyo"

#: gnome-vfs.keys.in.h:299
#, fuzzy
msgid "StarDraw drawing"
msgstr "Igishushanyo"

#: gnome-vfs.keys.in.h:300
#, fuzzy
msgid "StarImpress presentation"
msgstr "Iyerekana"

#: gnome-vfs.keys.in.h:301
#, fuzzy
msgid "StarMail file"
msgstr "IDOSIYE"

#: gnome-vfs.keys.in.h:302
#, fuzzy
msgid "StarMath document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:303
#, fuzzy
msgid "StarOffice extended metafile image"
msgstr "Byongerewe... Idosiye idasanzwe Ishusho"

#: gnome-vfs.keys.in.h:304
#, fuzzy
msgid "StarWriter document"
msgstr "Inyandiko"

# offmgr/source\offapp\dialog\optgdlg.src:OFA_TP_MISC.FT_STYLESHEET.text
#: gnome-vfs.keys.in.h:305
#, fuzzy
msgid "Style sheet"
msgstr "Urupapuro rw'Imisusire"

#: gnome-vfs.keys.in.h:306
#, fuzzy
msgid "Sun mu-law audio"
msgstr "mu"

#: gnome-vfs.keys.in.h:307
#, fuzzy
msgid "SunOS News font"
msgstr "Intego- nyuguti"

#: gnome-vfs.keys.in.h:308
#, fuzzy
msgid "Symbolic link"
msgstr "Ihuza"

#: gnome-vfs.keys.in.h:309
msgid "System"
msgstr "Sisitemu"

#: gnome-vfs.keys.in.h:310
#, fuzzy
msgid "TIFF image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:311
#, fuzzy
msgid "Tab-separated text document"
msgstr "Umwandiko Inyandiko"

#: gnome-vfs.keys.in.h:312
msgid "Tar archive"
msgstr ""

#: gnome-vfs.keys.in.h:313
#, fuzzy
msgid "Tar archive (Bzip2-compressed)"
msgstr "Byegeranijwe"

#: gnome-vfs.keys.in.h:314
#, fuzzy
msgid "Tar archive (Gzip-compressed)"
msgstr "Byegeranijwe"

#: gnome-vfs.keys.in.h:315
#, fuzzy
msgid "TarGA image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:316
#, fuzzy
msgid "Tcl script"
msgstr "IYANDIKA"

#: gnome-vfs.keys.in.h:317
#, fuzzy
msgid "TeX document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:318
#, fuzzy
msgid "TeX dvi document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:319
#, fuzzy
msgid "TeX font"
msgstr "Intego- nyuguti"

#: gnome-vfs.keys.in.h:320
#, fuzzy
msgid "TeX font metrics"
msgstr "Intego- nyuguti Bijyanye n'ipima"

#: gnome-vfs.keys.in.h:321
#, fuzzy
msgid "TeXInfo document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:322
msgid "Theme"
msgstr ""

#: gnome-vfs.keys.in.h:323
#, fuzzy
msgid "ToutDoux document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:324
#, fuzzy
msgid "Troff document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:325
#, fuzzy
msgid "Troff me input document"
msgstr "Iyinjiza Inyandiko"

#: gnome-vfs.keys.in.h:326
#, fuzzy
msgid "Troff mm input document"
msgstr "mm Iyinjiza Inyandiko"

#: gnome-vfs.keys.in.h:327
#, fuzzy
msgid "Troff ms input document"
msgstr "Iyinjiza Inyandiko"

#: gnome-vfs.keys.in.h:328
#, fuzzy
msgid "TrueType font"
msgstr "Intego- nyuguti"

#: gnome-vfs.keys.in.h:329
#, fuzzy
msgid "Truevision Targa image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:330
#, fuzzy
msgid "USENET news message"
msgstr "Amakuru Ubutumwa"

#: gnome-vfs.keys.in.h:331
#, fuzzy
msgid "Unidata netCDF document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:332
#, fuzzy
msgid "Unknown type"
msgstr "Ubwoko"

#: gnome-vfs.keys.in.h:333
msgid "User Interface"
msgstr "Ahagenewe Ukoresha"

#: gnome-vfs.keys.in.h:334
msgid "User Interface/Fonts"
msgstr ""

#: gnome-vfs.keys.in.h:335
msgid "Ustar archive"
msgstr ""

#: gnome-vfs.keys.in.h:336
#, fuzzy
msgid "V font"
msgstr "V Intego- nyuguti"

#: gnome-vfs.keys.in.h:337
msgid "VOC audio"
msgstr ""

#: gnome-vfs.keys.in.h:338
#, fuzzy
msgid "VRML document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:339
#, fuzzy
msgid "Verilog source code"
msgstr "Inkomoko ITEGEKONGENGA"

# 6474
#: gnome-vfs.keys.in.h:340
msgid "Video"
msgstr "inyerekanamashusho"

#: gnome-vfs.keys.in.h:341
#, fuzzy
msgid "Vivo video"
msgstr "Videwo..."

#: gnome-vfs.keys.in.h:342
#, fuzzy
msgid "WAIS source code"
msgstr "Inkomoko ITEGEKONGENGA"

#: gnome-vfs.keys.in.h:343
msgid "Wave audio"
msgstr ""

#: gnome-vfs.keys.in.h:344
#, fuzzy
msgid "Wavelet video"
msgstr "Videwo..."

#: gnome-vfs.keys.in.h:345
#, fuzzy
msgid "Web folder"
msgstr "Ububiko"

#: gnome-vfs.keys.in.h:346
#, fuzzy
msgid "Windows bitmap image"
msgstr "Bitimapu Ishusho"

#: gnome-vfs.keys.in.h:347
#, fuzzy
msgid "Windows icon image"
msgstr "Agashushondanga Ishusho"

#: gnome-vfs.keys.in.h:348
#, fuzzy
msgid "Windows metafile graphics"
msgstr "Idosiye idasanzwe Ibishushanyo"

#: gnome-vfs.keys.in.h:349
#, fuzzy
msgid "WordPerfect document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:350
#, fuzzy
msgid "X bitmap image"
msgstr "Bitimapu Ishusho"

#: gnome-vfs.keys.in.h:351
#, fuzzy
msgid "X window image"
msgstr "Idirishya Ishusho"

#: gnome-vfs.keys.in.h:352
#, fuzzy
msgid "XBEL bookmarks file"
msgstr "Ibirango IDOSIYE"

#: gnome-vfs.keys.in.h:353
#, fuzzy
msgid "XML document"
msgstr "Inyandiko"

#: gnome-vfs.keys.in.h:354
#, fuzzy
msgid "XPM image"
msgstr "Ishusho"

#: gnome-vfs.keys.in.h:355
#, fuzzy
msgid "Xbase database"
msgstr "Ububikoshingiro"

#: gnome-vfs.keys.in.h:356
#, fuzzy
msgid "Yacc grammar source code"
msgstr "Inkomoko ITEGEKONGENGA"

#: gnome-vfs.keys.in.h:357
#, fuzzy
msgid "Z shell script"
msgstr "Igikonoshwa IYANDIKA"

#: gnome-vfs.keys.in.h:358
msgid "Zip archive"
msgstr ""

#: gnome-vfs.keys.in.h:359
msgid "Zoo archive"
msgstr ""

#: gnome-vfs.keys.in.h:360
#, fuzzy
msgid "gettext translation"
msgstr "Umwandiko wahinduwe ururimi"

#: gnome-vfs.keys.in.h:361
msgid "iPod software"
msgstr ""

#: gnome-vfs.keys.in.h:362
#, fuzzy
msgid "xfig vector graphic"
msgstr "Igishushanyo"

# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
